package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project>{

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}
