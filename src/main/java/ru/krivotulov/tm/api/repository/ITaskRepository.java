package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task>{

    Task create(final String userId, String name);

    Task create(final String userId, String name, String description);

    List<Task> findAllByProjectId(final String userId, String projectId);

}
