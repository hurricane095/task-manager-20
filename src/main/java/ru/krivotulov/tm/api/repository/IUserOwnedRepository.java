package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

/**
 * IUserOwnedRepository
 *
 * @author Aleksey_Krivotulov
 */
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M delete(String userId, M model);

    M deleteById(String userId, String id);

    M deleteByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    int getSize(String userId);

}
