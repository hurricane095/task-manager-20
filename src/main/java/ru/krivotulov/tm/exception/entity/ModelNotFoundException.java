package ru.krivotulov.tm.exception.entity;

/**
 * ModelNotFoundException
 *
 * @author Aleksey_Krivotulov
 */
public class ModelNotFoundException extends AbstractEntityNotFoundException{

    public ModelNotFoundException() {
        super();
    }

}
