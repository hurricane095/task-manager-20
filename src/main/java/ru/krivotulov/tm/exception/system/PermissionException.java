package ru.krivotulov.tm.exception.system;

/**
 * PermissionException
 *
 * @author Aleksey_Krivotulov
 */
public final class PermissionException extends AbstractMethodError {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
