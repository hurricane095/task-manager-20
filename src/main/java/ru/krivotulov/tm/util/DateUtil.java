package ru.krivotulov.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    String PATTERN = "dd.MM.yyyy";

    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    static Date toDate(final String date) {
        if (date == null || date.isEmpty()) return null;
        try {
            return FORMATTER.parse(date);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    static String toString(final Date date) {
        if (date == null) return "";
        return FORMATTER.format(date);
    }

}
