package ru.krivotulov.tm.model;

/**
 * AbstractOwner
 *
 * @author Aleksey_Krivotulov
 */
public class AbstractUserOwnedModel extends AbstractModel{

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
