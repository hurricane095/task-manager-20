package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * TaskShowByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskShowByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-by-id";

    public static final String DESCRIPTION = "Show task by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final String userId = getUserId();
        final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }

}
