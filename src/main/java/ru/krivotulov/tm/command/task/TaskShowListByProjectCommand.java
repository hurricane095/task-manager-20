package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.List;

/**
 * TaskShowListByProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskShowListByProjectCommand extends AbstractTaskCommand {

    public static final String NAME = "task-show-list-by-project";

    public static final String DESCRIPTION = "Display task list by project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.readLine();
        final String userId = getUserId();
        final List<Task> taskList = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(taskList);
    }

}
