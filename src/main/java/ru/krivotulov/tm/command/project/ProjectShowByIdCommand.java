package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * ProjectShowByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectShowByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-show-by-id";

    public static final String DESCRIPTION = "Show project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final String userId = getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
