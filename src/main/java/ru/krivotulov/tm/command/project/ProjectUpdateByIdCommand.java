package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.util.TerminalUtil;

/**
 * ProjectUpdateByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-id";

    public static final String DESCRIPTION = "Update project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        final String userId = getUserId();
        getProjectService().updateById(userId, id, name, description);
    }

}
