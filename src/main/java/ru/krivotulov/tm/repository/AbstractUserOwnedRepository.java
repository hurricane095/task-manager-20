package ru.krivotulov.tm.repository;

import ru.krivotulov.tm.api.repository.IUserOwnedRepository;
import ru.krivotulov.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * AbstractUserRepository
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public M add(final String userId, M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> resultList = new ArrayList<>();
        for (final M m : models) {
            if (userId.equals(m.getUserId())) resultList.add(m);
        }
        return resultList;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> resultList = findAll(userId);
        resultList.sort(comparator);
        return resultList;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        for (final M m : models) {
            if (!id.equals(m.getId())) continue;
            if (!userId.equals(m.getUserId())) continue;
            return m;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M delete(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return deleteById(userId, model.getUserId());
    }

    @Override
    public M deleteById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M deleteByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize(final String userId) {
        int count = 0;
        for (final M m : models) {
            if (userId.equals(m.getId())) count++;
        }
        return count;
    }

}
